import 'package:anagram/anagram.dart';
import 'package:anagram/isogram.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _currentIndex = 0;
  String _titleAppBar = "Anagram";

  final List<Widget> _children = [
    AnagramPage(),
    IsogramPage(),
  ];

  List<String> _listTitleMenu = [
    "Anagram",
    "Isogram",
  ];

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
      _titleAppBar = _listTitleMenu[index];
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        child: Scaffold(
            appBar: AppBar(
              title: Text(_titleAppBar),
            ),
            body: _children[_currentIndex],
            bottomNavigationBar: bottomNavigationBarList()),
        onWillPop: null);
  }

  BottomNavigationBar bottomNavigationBarList() {
    return BottomNavigationBar(
        currentIndex: _currentIndex,
        onTap: onTabTapped,
        items: [
          bottomNavigationBarItem("Anagram"),
          bottomNavigationBarItem("Isogram"),
        ]);
  }

  BottomNavigationBarItem bottomNavigationBarItem(String item) {
    IconData _icon;
    if (item == "Anagram") {
      _icon = Icons.adb;
    } else if (item == "Isogram") {
      _icon = Icons.whatshot;
    }

    return BottomNavigationBarItem(
      icon: new Icon(
        _icon,
        color: Color(0xFF828d99),
      ),
      title: new Text(
        item,
        style: TextStyle(
            color: Color(0xFF828d99), fontFamily: "Sailec", fontSize: 12.0),
      ),
    );
  }
}
