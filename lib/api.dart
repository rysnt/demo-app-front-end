import 'dart:convert';

import 'package:http/http.dart' as http;

class Api {
  Future<String> getRandomText() async {
    final url = 'http://demo-app-222.herokuapp.com/anagram-random';
    final response = await http.get(url);
    var result = json.decode(response.body);
    return result['word'];
  }

  Future<Map> validateAnagram(Map body) async {
    final url = 'http://demo-app-222.herokuapp.com/anagram-validate';
    final response = await http.post(url, body: body);
    var result = jsonDecode(response.body);
    return result;
  }

  Future<bool> validateIsogram(Map body) async {
    final url = 'http://demo-app-222.herokuapp.com/isogram-validate';
    final response = await http.post(url, body: body);
    var result = jsonDecode(response.body);
    return result;
  }
}