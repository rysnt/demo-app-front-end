import 'package:anagram/api.dart';
import 'package:flutter/material.dart';

class AnagramPage extends StatefulWidget {
  @override
  _AnagramPageState createState() => _AnagramPageState();
}

class _AnagramPageState extends State<AnagramPage> {
  TextEditingController _textController = new TextEditingController();
  String value = "";

  @override
  void initState() {
    getRandomText();
  }

  Future<void> getRandomText() async {
    value = await Api().getRandomText();
    if(mounted) {
      setState(() {});
    }
  }

  void refresh() {
    getRandomText();
  }

  Future<void> validateAnagram() async {
    Map data = {"text1": value, "text2": _textController.text};
    var response = await Api().validateAnagram(data);
    showDialogResponse(response);
  }

  Future showDialogResponse(response) {
    return showDialog(
      context: context,
      builder: (BuildContext dialogContext) => new AlertDialog(
        content: new Text(
          response['word'] != null ? "You're Right!!" : "Wrong!!!",
          style: TextStyle(fontFamily: "Sailec", color: Color(0xff45525d)),
        ),
        actions: <Widget>[
          new FlatButton(
            onPressed: () {
              Navigator.pop(dialogContext);
            },
            child: new Text('OK!'),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            value,
            style: TextStyle(fontSize: 25),
          ),
          Container(
            padding: EdgeInsets.all(20),
            margin: EdgeInsets.only(top: 20, bottom: 20),
            child: TextField(
              style: TextStyle(fontSize: 20),
              controller: _textController,
            ),
          ),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                FlatButton(
                  color: Colors.yellow,
                  onPressed: refresh,
                  child: Text("Refresh"),
                ),
                Container(
                  width: 20,
                ),
                FlatButton(
                  color: Colors.green,
                  onPressed: validateAnagram,
                  child: Text("Validate"),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
