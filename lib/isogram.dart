import 'package:anagram/api.dart';
import 'package:flutter/material.dart';

class IsogramPage extends StatefulWidget {
  @override
  _IsogramPageState createState() => _IsogramPageState();
}

class _IsogramPageState extends State<IsogramPage> {
  TextEditingController _textController = new TextEditingController();
  String value = "";

  @override
  void initState() {
    getRandomText();
  }

  Future<void> getRandomText() async {
    value = await Api().getRandomText();
    if(mounted) {
      setState(() {});
    }
  }

  Future<void> validateAnagram(bool guess) async {
    Map data = {"text": value, "isIsogram": guess.toString()};
    var response = await Api().validateIsogram(data);
    showDialogResponse(response);
  }

  Future showDialogResponse(response) {
    return showDialog(
      context: context,
      builder: (BuildContext dialogContext) => new AlertDialog(
        content: new Text(
          response ? "You're Right!!" : "Wrong!!!",
          style: TextStyle(fontFamily: "Sailec", color: Color(0xff45525d)),
        ),
        actions: <Widget>[
          new FlatButton(
            onPressed: () async {
              await getRandomText();
              Navigator.pop(dialogContext);
            },
            child: new Text('OK!'),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            value,
            style: TextStyle(fontSize: 25),
          ),
          Container(height: 20,),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                FlatButton(
                  color: Colors.green,
                  onPressed: () {
                    validateAnagram(true);
                  },
                  child: Text("Yes"),
                ),
                FlatButton(
                  color: Colors.green,
                  onPressed: () {
                    validateAnagram(false);
                  },
                  child: Text("No"),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
